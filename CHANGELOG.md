# Changelog

Im Verlauf der Entwicklung werden in dieser Datei die Änderungen in den einzelnen Versionen festgehalten. Die Versionsnummern folgen dem Prinzip der [semantischen Versionierung](https://semver.org)

## [1.2.0] - 2020-07-24
### Added
- Unterkapitel zum Thema Dateitypen in Verbindung mit GPG und Zertifikaten

### Changed
- Einleitungstext beschreibt jetzt auch alle Kapitel

### Fixed
- Rechtschreib- und Grammatikfehler

## [1.1.0] - 2020-07-23
### Added
- Kapitel GPG4Win und asymmetrische Verschlüsselung
- CC-BY-NC-ND-4.0 Lizenz

## [1.0.0] - 2019-03-19
### Added
- Kapitel VeraCrypt
- Kapitel KeePass 2
- Kapitel Paranoia Text Encryption
- Kapitel S.S.E. File Encryptor